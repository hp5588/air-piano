# Source code of research publication
## Feel the pressure: a haptic-feedback device for wearable musical instrument interaction
https://dl.acm.org/doi/abs/10.1145/3341162.3343806

Unity implementation that converts the hand motion to pressure feedback on the artist's arm.

Here we have a scenario where the artist wants to adjust the volume of music virtually in the air before plays the music.
However, he/she has no clue what the current value is before the music is present.
This could cause trouble having either too loud or little volume once the music is out.

In our demo, the volume of music can be adjusted by moving the fist in the air. The motion of the hand is captured by Leap Motion and further passed to Unity for processing, thus change the volume. Volume information will also be interpreted as appropriate pressure that asserts on the artist's arm. 

The source code of the actuator is not included in this repository.