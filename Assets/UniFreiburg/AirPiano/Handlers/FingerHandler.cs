﻿using System.Collections.Generic;
using Leap;
using UnityEngine;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;

namespace UniFreiburg.AirPiano.Handlers
{
    public class FingerHandler : MonoBehaviour {
        [FormerlySerializedAs("keyClips")] 
        public AudioClip[] KeyClips;
    

        private List<AudioSource> _keyAudioSources;
        private AudioSource _soundDo;


        private void Awake()
        {
            _keyAudioSources = new List<AudioSource>();
            _soundDo = GetComponentInParent<AudioSource>();

        
            GameObject audioSourcesObject = GameObject.Find("AudioSources");
            // init  the sound source for each finger
            foreach (var clip in KeyClips)
            {
                var audioSource = audioSourcesObject.AddComponent<AudioSource>();
                audioSource.clip = clip;
                audioSource.playOnAwake = false;
                _keyAudioSources.Add(audioSource);
            }
        }

        // Use this for initialization
        private void Start () {
        
      
        }

        public void OnTouched(Object sender)
        {
            _soundDo.Play();
        }

        public void OnPalmActive()
        {
            print("Palm Activated");
        }
        public void OnPalmDeactive()
        {
            print("Palm Activated");
        }


        public void OnFingerBended(Finger.FingerType fingerType, Hand hand)
        {

            if (hand.IsRight)
            {
                AudioSource audioSource = _keyAudioSources[fingerType.indexOf()];
                if (!audioSource.isPlaying)
                {
                    print(fingerType);
                    audioSource.Play();
                }
            }
 

//        switch (fingerType){
//            case Finger.FingerType.TYPE_THUMB:
//                break;
//            case Finger.FingerType.TYPE_INDEX:
//                if (!_soundDo.isPlaying)
//                {
//                    _soundDo.Play();
//                }
//                break;
//            case Finger.FingerType.TYPE_MIDDLE:
//                break;
//            case Finger.FingerType.TYPE_RING:
//                break;
//            case Finger.FingerType.TYPE_PINKY:
//                break;
//            case Finger.FingerType.TYPE_UNKNOWN:
//                break;
//        }
        }

        // Update is called once per frame
        void Update () {
		
        }
    }
}
