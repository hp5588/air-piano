﻿using Leap;
using UniFreiburg.AirPiano.Models.Command;
using UnityEngine;

namespace UniFreiburg.AirPiano.Handlers
{
	public class HandBoundaryHandler : MonoBehaviour
	{
		[Tooltip("Assign a gameobject which has WatchController component")]
		public GameObject WatchGameObject;
		
		[Tooltip("sound effect to play when passing boarder")]
		public AudioClip BorderSoundClip;


		private WatchController _watchController;
		private AudioSource _audioSource;
		
		// Use this for initialization
		void Start ()
		{
			_watchController = WatchGameObject.GetComponent<WatchController>();
			_audioSource = gameObject.AddComponent<AudioSource>();
			_audioSource.clip = BorderSoundClip;
			_audioSource.playOnAwake = false;
		}
	
		// Update is called once per frame
		void Update () {
		
		}

		public void OnHandOutOfBoundary(Hand hand)
		{
			if (WatchGameObject != null)
			{
				Vibration vibration = new Vibration {length = 10, strength = 55};
				_watchController.SendCommand(vibration);
				_audioSource.Play();
			}
		}
		public void OnHandInBoundary(Hand hand)
		{
			if (WatchGameObject != null)
			{
				Vibration vibration = new Vibration {length = 22, strength = 11};
				_watchController.SendCommand(vibration);
				_audioSource.Play();
			}
		}
	}
}
