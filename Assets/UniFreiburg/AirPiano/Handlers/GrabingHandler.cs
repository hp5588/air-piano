﻿using Leap;
using Leap.Unity;
using UnityEngine;
using UnityEngine.UI;

namespace UniFreiburg.AirPiano.Handlers
{
	public class GrabingHandler : MonoBehaviour {
	
		// state variables
		private bool _leftHandHold;
	
		// initial reference values
		private Vector3 _palmInitNormal;
		private float _audioSourceInitVolume;

		// scene resources
		private Text _rotateAngelLText;
		private AudioSource[] _audioSources;


		// Use this for initialization
		void Start () {
			_rotateAngelLText = GameObject.Find("RotateAngleL").GetComponent<Text>();
			_audioSources = GameObject.Find("AudioSources").GetComponents<AudioSource>();
		}
	
	
		// Update is called once per frame
		void Update () {
		
		}
	
	
		public void OnHandGrab(Hand hand)
		{
			if (hand.IsLeft)
			{
			
				if (!_leftHandHold)
				{
					// record reference value
					_palmInitNormal = hand.PalmNormal.ToVector3();
					_audioSourceInitVolume = _audioSources[0].volume;
					_leftHandHold = true;
				
				}
				else
				{
					//check the angle between init state and now
					Vector3 palmNormal = hand.PalmNormal.ToVector3();
					float angle = Vector3.SignedAngle(_palmInitNormal, palmNormal, hand.Direction.ToVector3());

                    
					//map angel to volume offset
					float volumeOffset = Mathf.Lerp(-1, 1, (angle + 90)  / 180.0f);
					_rotateAngelLText.text = volumeOffset.ToString();

					foreach (var audioSource in _audioSources)
					{
						audioSource.volume = _audioSourceInitVolume + volumeOffset;
					}

				}
			}

		}

		public void OnHandUngrab(Hand hand)
		{
			if (hand.IsLeft)
			{
				_leftHandHold = false;
			}
		}

	}
}