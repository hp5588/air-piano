using System;
using System.Collections.Generic;
using Leap;
using Leap.Unity;
using UnityEngine;
using UnityEngine.Events;

namespace UniFreiburg.AirPiano
{
    
    [System.Serializable]
    public class BoundaryEvent : UnityEvent<Hand>
    {
    }

    
    public class HandPositionDetector : AirPianoDetectorBase
    {
        private enum BoundaryState
        {
            In,
            Out
        }

        
        public BoundaryEvent OnOut;
        public BoundaryEvent OnIn;
        
        [Tooltip("The left boundary before event  is trigger")]
        public GameObject LeftBoundary;
        
        [Tooltip("The right boundary before event  is trigger")]
        public GameObject RightBoundary;

        private BoundaryState _boundaryState = BoundaryState.In;

        private void Update()
        {            
            // get lastest hand information
            UpdateHands();

            foreach (var hand in hands)
            {
                if (hand.IsRight)
                {
                    BoundaryState boundaryState = CheckBoundary(hand.PalmPosition.ToVector3());

                    if (boundaryState != _boundaryState)
                    {
                        switch (boundaryState)
                        {
                            case BoundaryState.In:
                                OnIn.Invoke(hand);
                                break;
                            case BoundaryState.Out:
                                OnOut.Invoke(hand);
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                        _boundaryState = boundaryState;
                    }
                }
            }
            
        }

        private BoundaryState CheckBoundary(Vector3 point)
        {
            Vector3 leftPoint = LeftBoundary.transform.position;
            Vector3 rightPoint = RightBoundary.transform.position;

            Vector3 centerLine = leftPoint - rightPoint;
            Vector3 rLine = point - rightPoint;
            Vector3 lLine = leftPoint - point;

            float rAngle = Vector3.Angle(rLine, centerLine);
            float lAngle = Vector3.Angle(lLine, centerLine);

            if (rAngle > 90 | lAngle > 90)
            {
                return BoundaryState.Out;
            }

            return BoundaryState.In;

        }
        
        private void Awake()
        {
           
        }
    }


}