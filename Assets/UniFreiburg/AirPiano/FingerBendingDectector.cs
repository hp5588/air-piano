﻿using System.Collections.Generic;
using Leap;
using Leap.Unity;
using UnityEngine;
using UnityEngine.Events;

namespace UniFreiburg.AirPiano
{
    
    [System.Serializable]
    public class FingerBendingEvent : UnityEvent<Finger.FingerType, Hand>
    {
    }
    [System.Serializable]
    public class GrabingEvent : UnityEvent<Hand>
    {
    }
    
    
    public class FingerBendingDectector : Detector
    {
        [Tooltip("Dispatched when finger pass the detection border.")]
        public FingerBendingEvent OnEnter;
        
        [Tooltip("Dispatched when finger leave the detection border.")]
        public FingerBendingEvent OnExit;
        
        [Tooltip("Dispatched when hand graping is detected")]
        public GrabingEvent OnGrab;
        
        [Tooltip("Dispatched when hand release from graping")]
        public GrabingEvent OnUngrab;
        
        [Tooltip("The right hand model to watch.")]
        public HandModelBase RightHandModel = null;        
        
        [Tooltip("The left hand model to watch.")]
        public HandModelBase LeftHandModel = null;

        public double GrabStrengthThreshold = 0.8;

        private double _lastGrabStrength;
        
        
        // Use this for initialization
        void Start()
        {
           
        }
        
    private void Update()
        {
            Hand rightHand;
            Hand leftHand;
            List<Hand> hands = new List<Hand>();

            if (RightHandModel != null && RightHandModel.IsTracked)
            {
                rightHand = RightHandModel.GetLeapHand();
                hands.Add(rightHand);
            }            
            
            if (LeftHandModel != null && LeftHandModel.IsTracked)
            {
                leftHand = LeftHandModel.GetLeapHand();
                hands.Add(leftHand);
            }


            foreach (var hand in hands)
            {
                if (hand.GrabStrength > GrabStrengthThreshold)
                {
                    OnGrab.Invoke(hand);
                }
                else
                {
                    OnUngrab.Invoke(hand);
                }
                
                Debug.DrawRay(hand.PalmPosition.ToVector3(), hand.PalmNormal.ToVector3(), Color.green);

                foreach (var finger in hand.Fingers)
                {
                    Vector3 palmNormal = hand.PalmNormal.ToVector3();
                    Vector3 fingerMcpDirection;
//                    fingerMcpDirection = finger.bones[1].Direction.ToVector3();
                    
                    Bone bone;
                    // thumb has different structure
                    if (finger.Type == Finger.FingerType.TYPE_THUMB)
                    {
                        bone = finger.bones[2];
                    }
                    else
                    {
                        bone = finger.bones[1];
                    }                
                    fingerMcpDirection = bone.Direction.ToVector3();
                    
                    //debug use
                    Debug.DrawRay(bone.PrevJoint.ToVector3(), fingerMcpDirection, Color.magenta);
                
                    float angle = Vector3.Angle(palmNormal, fingerMcpDirection);

                    if (finger.Type == Finger.FingerType.TYPE_THUMB)
                    {
                        if (angle < 60)
                        {
                            OnEnter.Invoke(finger.Type, hand);
                        }
                    }
                    else
                    {
                        if (angle < 45)
                        {
                            OnEnter.Invoke(finger.Type, hand);
//                        print(angle);
                        }

                    }

                    
                }
            }
           
               
            
        }
    }
}
