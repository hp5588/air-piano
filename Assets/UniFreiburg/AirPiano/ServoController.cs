﻿using System;
using System.Net;
using System.Net.Sockets;
using Leap;
using Leap.Unity;
using UniFreiburg.AirPiano.Models.Command;
using UnityEngine;

namespace UniFreiburg.AirPiano
{
	public class ServoController : ControllerBase {
		[Tooltip("The right hand model to watch.")]
		public HandModelBase RightHandModel;        
        
		[Tooltip("The left hand model to watch.")]
		public HandModelBase LeftHandModel;

		public GameObject OriginReference;
		
		// Use this for initialization
		void Start () {
			InitConnection();
			// send regulator info
			Regulator regulator = new Regulator();
			regulator.kP = 0.89;
			regulator.kI = 0.05;
			regulator.kD = 200.4;
			regulator.sampleTime = 100;

			SendCommand(new RegulatorWrapper(regulator));
		}
	
		// Update is called once per frame
		void Update () {
			Position position = new Position();
			if (RightHandModel.IsTracked)
			{
				Hand hand = RightHandModel.GetLeapHand();
				position.absoluteX = hand.PalmPosition.x;
				position.absoluteY = hand.PalmPosition.y;
				position.absoluteZ = hand.PalmPosition.z;				

				PositionWrapper wrapper = new PositionWrapper(position);

				SendCommand(wrapper);
			}
		}
		
		
	}
}
