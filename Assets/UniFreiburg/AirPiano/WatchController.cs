﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UniFreiburg.AirPiano.Models.Command;
using UnityEngine;

namespace UniFreiburg.AirPiano
{
	public class WatchController : ControllerBase
	{

		// Use this for initialization
		void Start () {
			InitConnection();
		}
		

		public void SendCommand(CommandBase command)
		{
			byte[] bytes = command.ToBytes();
			__udpClient.Send(bytes, bytes.Length, remoteIpEndPoint);
			
			print("data sent: " + bytes);
		}
	}
}
