using System;
using System.Text;
using UnityEngine;

namespace UniFreiburg.AirPiano.Models.Command
{
    [Serializable]
    public class CommandBase
    {
        public enum Command
        {
            Vibrate,
            Position,
            Read
        }

        public byte[] ToBytes()
        {
            string json = JsonUtility.ToJson(this);
            return Encoding.ASCII.GetBytes(json);
        }

    }
}