using System;

namespace UniFreiburg.AirPiano.Models.Command
{
    [Serializable]
    public class Position : CommandBase
    {
        [NonSerialized] public Command command;

        public float absoluteX;
        public float absoluteY;
        public float absoluteZ;

    }
}