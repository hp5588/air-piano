using System;

namespace UniFreiburg.AirPiano.Models.Command
{
    [Serializable]
    public class Regulator : CommandBase
    {
        public double kP;
        public double kI;
        public double kD;
        public double sampleTime;

    }
}