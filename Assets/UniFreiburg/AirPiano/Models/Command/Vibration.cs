namespace UniFreiburg.AirPiano.Models.Command
{
    public class Vibration: CommandBase
    {
        public Command command;
        public float length = 0;
        public float strength = 0;

        public Vibration()
        {
            command = Command.Vibrate;
        }

    }
}