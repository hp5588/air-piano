using System;

namespace UniFreiburg.AirPiano.Models.Command
{
    [Serializable]
    public class RegulatorWrapper : CommandBase
    {
        public Regulator regulator;

        public RegulatorWrapper(Regulator regulator)
        {
            this.regulator = regulator ?? new Regulator();
        }
    }
}