using System;

namespace UniFreiburg.AirPiano.Models.Command
{
    [Serializable]
    public class PositionWrapper : CommandBase
    {
        public Position position;

        public PositionWrapper(Position position)
        {
            this.position = position ?? new Position();
        }
    }
}