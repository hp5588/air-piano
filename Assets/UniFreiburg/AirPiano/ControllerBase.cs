using System;
using System.Net;
using System.Net.Sockets;
using UniFreiburg.AirPiano.Models.Command;
using UnityEngine;

namespace UniFreiburg.AirPiano
{
    public class ControllerBase : MonoBehaviour
    {
        protected UdpClient __udpClient;
        protected IPEndPoint remoteIpEndPoint;
        public String _remoteIp;
        public String _remotePort;

        protected void InitConnection()
        {
            __udpClient = new UdpClient();
            remoteIpEndPoint= new IPEndPoint(IPAddress.Parse(_remoteIp), int.Parse(_remotePort));
        }

        protected bool SendCommand(CommandBase command)
        {
            if (__udpClient != null)
            {
                var bytes = command.ToBytes();
                __udpClient.Send(bytes, bytes.Length, remoteIpEndPoint);
                return true;
            }

            return false;
        }

    }
     
}