using System.Collections.Generic;
using Leap;
using Leap.Unity;
using UnityEngine;

namespace UniFreiburg.AirPiano
{
    public class AirPianoDetectorBase : Detector
    {
        [Tooltip("The right hand model to watch.")]
        public HandModelBase RightHandModel = null;        
        
        [Tooltip("The left hand model to watch.")]
        public HandModelBase LeftHandModel = null;
        
        
        protected List<Hand> hands;


        protected void UpdateHands()
        {
            hands = new List<Hand>();

            Hand rightHand;
            Hand leftHand;

            if (RightHandModel != null && RightHandModel.IsTracked)
            {
                rightHand = RightHandModel.GetLeapHand();
                hands.Add(rightHand);
            }            
            
            if (LeftHandModel != null && LeftHandModel.IsTracked)
            {
                leftHand = LeftHandModel.GetLeapHand();
                hands.Add(leftHand);
            }
        }

    }
}